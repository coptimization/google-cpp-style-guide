# Google Cpp Style Guide

A ideia seria criar uma aula onde estaria contido ideias centrais do Cpp Style Guide para que no futuro a adesão de novos programadores ao projeto seja feita de forma mais natural

## Iniciando

### Apresentação


[Aula](https://drive.google.com/file/d/1spX97SPehO-nYg6mZMpQLdMgeunMyyLH/view?usp=sharing) - Apresentação de slides

## Autores

* **Thiago Jansen Sampaio** - *Colaborador* - [yABSampaio](https://gitlab.com/yABSampaio)

Veja a lista de todos colaboradores do projeto [Colaboradores](https://gitlab.com/coptimization/cleaning-experiment/-/project_members).

## Licença
Este projeto é licenciado pela licensa do MIT - veja em [LICENSE.md](LICENSE.md) para mais detalhes.